<?php

namespace Procontext\WebinarApi;

use Procontext\WebinarApi\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

class QuestionParams
{
    /**
     * Id сессии (обязательный). Берётся в ЛК
     *
     * @var string
     */
    public $eventSessionId;

    /**
     * Сайт с которого отправлен запрос
     *
     * @var string
     */
    public $referer;

    /**
     * Текст вопроса
     *
     * @var string
     */
    public $text = '';

    protected $properties;
    protected $errors;

    public function __construct(array $formData)
    {
        if(!$this->eventSessionId = env('WEBINAR_EVENT_SESSION_ID')) {
            $this->errors[] = 'Id сессии не задан в конфигурации';
        }

        $ssl = env('SSL', false);
        if(!$url = env('URL')) {
            $this->errors[] = 'Url не задан в конфигурации';
        } else {
            $this->referer = ($ssl ? 'https://' : 'http://') . $url;
        }

        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property])) {
                $this->{$property} = $formData[$property];
            } elseif(!isset($this->{$property})) {
                $this->errors[] = "Параметр " . $property . " не задан";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        $formData['text'] = $formData['question'] ?: '';
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            if(in_array($property, ['referer'])) {
                continue;
            }
            $formData[$property] = $this->{$property};
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
