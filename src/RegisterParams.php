<?php

namespace Procontext\WebinarApi;

use Procontext\WebinarApi\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

class RegisterParams
{
    /**
     * Id сессии (обязательный). Берётся в ЛК
     *
     * @var string
     */
    public $eventSessionId;

    /**
     * Сайт с которого отправлен запрос
     *
     * @var string
     */
    public $referer;

    /**
     * Почта пользователя
     *
     * @var string
     */
    public $email = '';

    /**
     * Имя пользователя
     *
     * @var string
     */
    public $name = '';

    /**
     * Фамилия пользователя
     *
     * @var string
     */
    public $secondName = '';

    /**
     * Номер телефона
     *
     * @var string
     */
    public $phone = '';

    /**
     * Город
     *
     * @var string
     */
    public $region = '';

    /**
     * Сайт организации
     *
     * @var string
     */
    public $organization = '';

    /**
     * Должность
     *
     * @var string
     */
    public $position = '';

    /**
     * Флаг определяет отправку письма "Вы зарегистрированы на вебинар".
     *
     * @var string
     */
    public $sendEmail = 'true';

    protected $additionalFields;
    protected $properties;
    protected $errors;

    public function __construct(array $formData, array $additionalFields)
    {
        if(!$this->eventSessionId = env('WEBINAR_EVENT_SESSION_ID')) {
            $this->errors[] = 'Id сессии не задан в конфигурации';
        }

        $ssl = env('SSL', false);
        if(!$url = env('URL')) {
            $this->errors[] = 'Url не задан в конфигурации';
        } else {
            $this->referer = ($ssl ? 'https://' : 'http://') . $url;
        }

        $this->additionalFields = $additionalFields;

        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property])) {
                $this->{$property} = $formData[$property];
            } elseif(!isset($this->{$property})) {
                $this->errors[] = "Параметр " . $property . " не задан";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        $formData['secondName'] = $formData['last_name'] ?: '';
        $formData['organization'] = $formData['site_link'] ?: '';
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            if(in_array($property, ['phone', 'region', 'referer'])) {
                continue;
            }
            $formData[$property] = $this->{$property};
        }
        $formData['additionalFields'] = [];
        foreach ($this->additionalFields as $field) {
            switch ($field['label']) {
                case 'Телефон':
                    $formData['additionalFields'][$field['key']] = $this->phone;
                    break;
                case 'Город':
                    $formData['additionalFields'][$field['key']] = $this->region;
                    break;
                case 'Должность':
                    $formData['additionalFields'][$field['key']] = $this->position;
                    break;
                case 'Компания':
                case 'Ссылка на сайт компании':
                    $formData['additionalFields'][$field['key']] = $this->organization;
                    break;
            }
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
