<?php

namespace Procontext\WebinarApi;

use Procontext\WebinarApi\Exception\WebinarApiException;
use Procontext\WebinarApi\Exception\WebinarApiQuestionsResponseException;
use Procontext\WebinarApi\Exception\ValidationException;
use Procontext\WebinarApi\Exception\WebinarApiDisabledException;
use Procontext\WebinarApi\Exception\WebinarApiRegistrationResponseException;
use Procontext\WebinarApi\Exception\WebinarApiTokenException;

class WebinarApi
{
    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_FAILED = 'failed';

    private $enable;

    public function __construct()
    {
        $this->enable = env('WEBINAR_ENABLE', false);
    }

    public function getEvents()
    {
        $this->enableGuard();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($curl, CURLOPT_URL, 'https://userapi.webinar.ru/v3/organization/events/schedule');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return $response;
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }

    public function getAdditionalFields()
    {
        $this->enableGuard();

        $eventSessionId = env('WEBINAR_EVENT_SESSION_ID');

        if(!$eventSessionId) {
            throw new ValidationException(["Id сессии не задан в конфигурации"]);
        }

        $curl = curl_init();
        $url = 'https://userapi.webinar.ru/v3/eventsessions/' . $eventSessionId;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return array_get($response, 'additionalFields', []);
    }


    /**
     * @return array
     *    [
     *      "status"      => "success",
     *      "id"          => 1743483,
     *      "text"        => "Привет. Когда начнем?",
     *      "isModerated" => true,
     *      "isAnswered"  => true,
     *      "createAt"    => "2019-07-10T19:39:24+0300",
     *      "user"     => [
     *          "id"        => 102,
     *          "nickname"  => "Младший сотрудник тех.поддержки"
     *      ],
     *      "answers"     => [
     *           [
     *              "id"       => 1743483,
     *              "createAt" => "2019-07-10T19:39:24+0300",
     *              "text"     => "Спасибо за вопрос. Начнем ровно в восемь вечера.",
     *              "user"     => [
     *                  "id"        => 1029,
     *                  "nickname"  => "Специалист тех.поддержки"
     *              ],
     *           ]
     *      ]
     *    ]
     */
    public function sendQuestion(QuestionParams $params): array
    {
        $this->enableGuard();

        $url = 'https://userapi.webinar.ru/v3/eventsessions/' . $params->eventSessionId . '/questions';

        $curl = curl_init($url);
        curl_setopt_array(
            $curl,
            [
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => $this->getHeaders(),
                CURLOPT_REFERER => $params->referer,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => http_build_query($params->export()),
                CURLOPT_CONNECTTIMEOUT => 30
            ]
        );

        $response = json_decode(curl_exec($curl), true);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($response === false) {
            $response = [
                'status' => self::RESPONSE_FAILED,
                'reason' => curl_error($curl)
            ];
        } elseif($httpCode >= 300) {
            $response = [
                'status' => self::RESPONSE_FAILED,
                'reason' => $response
            ];
        } else {
            $response['status'] = isset($response['id']) && !empty($response['id'])
                ? self::RESPONSE_SUCCESS
                : self::RESPONSE_FAILED;
        }
        curl_close($curl);

       if($response['status'] !== self::RESPONSE_SUCCESS) {
           throw new WebinarApiQuestionsResponseException($response);
       }
        return $response;
    }

    /**
     * @return array
     *    [
     *      "status"          => "success",
     *      "participationId" => 1743483,
     *      "link"            => "https://events.webinar.ru/supportservice/2356695/97683fed47e6f1cfbe325664bfce94c23",
     *      "contactId"       => 48584027 (Возвращается только в случае, если Email ранее не был зарегистрирован на мероприятия организации)
     *    ]
     */
    public function sendRegister(RegisterParams $params): array
    {
        $url = 'https://userapi.webinar.ru/v3/eventsessions/' . $params->eventSessionId . '/register';

        $curl = curl_init($url);
        curl_setopt_array(
            $curl,
            [
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => $this->getHeaders(),
                CURLOPT_REFERER => $params->referer,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => http_build_query($params->export()),
                CURLOPT_CONNECTTIMEOUT => 30
            ]
        );

        $response = json_decode(curl_exec($curl), true);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($response === false) {
            $response = [
                'status' => self::RESPONSE_FAILED,
                'reason' => curl_error($curl)
            ];
        } elseif($httpCode >= 300) {
            $response = [
                'status' => self::RESPONSE_FAILED,
                'reason' => $response
            ];
        } else {
            $response['status'] = isset($response['participationId']) && !empty($response['participationId'])
                ? self::RESPONSE_SUCCESS
                : self::RESPONSE_FAILED;
        }

        if($response['status'] !== self::RESPONSE_SUCCESS) {
            throw new WebinarApiRegistrationResponseException($response);
        }
        return $response;
    }

    private function enableGuard(): void
    {
        if (!$this->enable) {
            throw new WebinarApiDisabledException();
        }
    }

    private function getHeaders()
    {
        $token = env('WEBINAR_TOKEN');

        if (!$token) {
            throw new WebinarApiTokenException();
        }

        return [
            'x-auth-token: ' . $token,
            'Content-Type: application/x-www-form-urlencoded'
        ];
    }
}
