<?php

namespace Procontext\WebinarApi\Exception;

use Throwable;

class ValidationException extends WebinarApiException
{
    protected $messageBag;

    public function __construct($messageBag = [], $message = 'Ошибка валидации параметров Webinar API', $code = 400, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->messageBag = $messageBag;
    }

    public function getMessageBag(): array
    {
        return $this->messageBag;
    }
}

