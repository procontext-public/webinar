<?php

namespace Procontext\WebinarApi\Exception;

use Throwable;

class WebinarApiTokenException extends WebinarApiException
{
    public function __construct($message = 'Токен api не задан в конфигурации', $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}