<?php

namespace Procontext\WebinarApi\Exception;

use Throwable;

class WebinarApiQuestionsResponseException extends WebinarApiException
{
    protected $response;

    public function __construct($response = [], $message = 'Ошибка ответа Webinar API (Метод Questions)', $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}